package com.excilys.librarymanager.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.excilys.librarymanager.exception.ServiceException;
import com.excilys.librarymanager.modele.Membre;
import com.excilys.librarymanager.service.MembreService;
import com.excilys.librarymanager.service.MembreServiceImpl;

@SuppressWarnings("serial")
public class MembreDeleteServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = -1;
		MembreService membreService = MembreServiceImpl.getInstance();

		try {
			id = Integer.parseInt(request.getParameter("id"));
		} catch(IllegalArgumentException e) {
			throw new ServletException("Identifiant de membre invalide");
		}

		try {
			Membre membre = membreService.getById(id);
			request.setAttribute("membre", membre);
		} catch (ServiceException e) {
			throw new ServletException(e);
		}

		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/View/membre_delete.jsp");
		rd.forward(request,response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MembreService membreService = MembreServiceImpl.getInstance();
		int id = -1;

		try {
			id = Integer.parseInt(request.getParameter("id"));
		} catch(IllegalArgumentException e) {
			throw new ServletException("Identifiant de membre invalide");
		}

		try {
			membreService.delete(id);
		} catch (ServiceException e) {
			throw new ServletException(e);
		}

		response.sendRedirect("/LibraryManager/membre_list");
	}
}
