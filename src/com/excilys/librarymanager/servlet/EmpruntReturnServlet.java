package com.excilys.librarymanager.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.excilys.librarymanager.exception.ServiceException;
import com.excilys.librarymanager.modele.Emprunt;
import com.excilys.librarymanager.service.EmpruntService;
import com.excilys.librarymanager.service.EmpruntServiceImpl;

@SuppressWarnings("serial")
public class EmpruntReturnServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EmpruntService empruntService = EmpruntServiceImpl.getInstance();
		List<Emprunt> emprunts = new ArrayList<>();

		try {
			int id = Integer.parseInt(request.getParameter("id"));
			request.setAttribute("id", id);
		} catch(IllegalArgumentException e) {
			// do nothing
		}

		try {
			emprunts = empruntService.getListCurrent();
			request.setAttribute("emprunts", emprunts);
		} catch (ServiceException e) {
			throw new ServletException(e);
		}

		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/View/emprunt_return.jsp");
		rd.forward(request,response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = -1;
		EmpruntService empruntService = EmpruntServiceImpl.getInstance();

		try {
			id = Integer.parseInt(request.getParameter("id"));
		} catch(IllegalArgumentException e) {
			throw new ServletException("Identifiant d'emprunt invalide");
		}

		try {
			empruntService.returnBook(id);
		} catch (ServiceException e) {
			throw new ServletException(e);
		}

		response.sendRedirect("/LibraryManager/emprunt_list");
	}
}
