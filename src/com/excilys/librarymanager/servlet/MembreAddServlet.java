package com.excilys.librarymanager.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.excilys.librarymanager.exception.ServiceException;
import com.excilys.librarymanager.service.MembreService;
import com.excilys.librarymanager.service.MembreServiceImpl;

@SuppressWarnings("serial")
public class MembreAddServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/View/membre_add.jsp");
		rd.forward(request,response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MembreService membreService = MembreServiceImpl.getInstance();
		int id = -1;
		String nom = null;
		String prenom = null;
		String adresse = null;
		String email = null;
		String telephone = null;

		nom	   = request.getParameter("nom");
		prenom	= request.getParameter("prenom");
		adresse   = request.getParameter("adresse");
		email	 = request.getParameter("email");
		telephone = request.getParameter("telephone");

		try {
			id = membreService.create(nom, prenom, adresse, email, telephone);
		} catch (ServiceException e) {
			throw new ServletException(e);
		}

		response.sendRedirect("/LibraryManager/membre_details?id=" + id);
	}
}
