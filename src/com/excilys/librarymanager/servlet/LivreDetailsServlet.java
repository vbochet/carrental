package com.excilys.librarymanager.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.excilys.librarymanager.exception.ServiceException;
import com.excilys.librarymanager.modele.Emprunt;
import com.excilys.librarymanager.modele.Livre;
import com.excilys.librarymanager.service.EmpruntService;
import com.excilys.librarymanager.service.EmpruntServiceImpl;
import com.excilys.librarymanager.service.LivreService;
import com.excilys.librarymanager.service.LivreServiceImpl;

@SuppressWarnings("serial")
public class LivreDetailsServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = -1;
		LivreService livreService = LivreServiceImpl.getInstance();
		EmpruntService empruntService = EmpruntServiceImpl.getInstance();

		try {
			id = Integer.parseInt(request.getParameter("id"));
		} catch(IllegalArgumentException e) {
			throw new ServletException("Identifiant de livre invalide");
		}

		try {
			Livre livre = livreService.getById(id);
			request.setAttribute("livre", livre);

			List<Emprunt> emprunts = empruntService.getListCurrentByLivre(id);
			request.setAttribute("emprunts", emprunts);
		} catch (ServiceException e) {
			throw new ServletException(e);
		}

		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/View/livre_details.jsp");
		rd.forward(request,response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LivreService livreService = LivreServiceImpl.getInstance();
		int id = -1;
		String titre = null;
		String auteur = null;
		String isbn = null;

		try {
			id = Integer.parseInt(request.getParameter("id"));
		} catch(IllegalArgumentException e) {
			throw new ServletException("Identifiant de livre invalide");
		}

		titre  = request.getParameter("titre");
		auteur = request.getParameter("auteur");
		isbn   = request.getParameter("isbn");

		try {
			Livre livre = new Livre(id, titre, auteur, isbn);
			livreService.update(livre);
		} catch (ServiceException e) {
			throw new ServletException(e);
		}

		response.sendRedirect("/LibraryManager/livre_details?id=" + id);
	}
}
