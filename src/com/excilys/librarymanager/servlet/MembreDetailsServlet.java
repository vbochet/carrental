package com.excilys.librarymanager.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.excilys.librarymanager.exception.ServiceException;
import com.excilys.librarymanager.modele.Abonnement;
import com.excilys.librarymanager.modele.Emprunt;
import com.excilys.librarymanager.modele.Membre;
import com.excilys.librarymanager.service.EmpruntService;
import com.excilys.librarymanager.service.EmpruntServiceImpl;
import com.excilys.librarymanager.service.MembreService;
import com.excilys.librarymanager.service.MembreServiceImpl;

@SuppressWarnings("serial")
public class MembreDetailsServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = -1;
		MembreService membreService = MembreServiceImpl.getInstance();
		EmpruntService empruntService = EmpruntServiceImpl.getInstance();

		try {
			id = Integer.parseInt(request.getParameter("id"));
		} catch(IllegalArgumentException e) {
			throw new ServletException("Identifiant de membre invalide");
		}

		try {
			Membre membre = membreService.getById(id);
			request.setAttribute("membre", membre);

			List<Emprunt> emprunts = empruntService.getListCurrentByMembre(id);
			request.setAttribute("emprunts", emprunts);
		} catch (ServiceException e) {
			throw new ServletException(e);
		}

		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/View/membre_details.jsp");
		rd.forward(request,response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MembreService membreService = MembreServiceImpl.getInstance();
		int id = -1;
		String nom = null;
		String prenom = null;
		String adresse = null;
		String email = null;
		String telephone = null;
		Abonnement abonnement = Abonnement.BASIC;

		try {
			id = Integer.parseInt(request.getParameter("id"));
		} catch(IllegalArgumentException e) {
			throw new ServletException("Identifiant de membre invalide");
		}

		nom		= request.getParameter("nom");
		prenom	 = request.getParameter("prenom");
		adresse	= request.getParameter("adresse");
		email	  = request.getParameter("email");
		telephone  = request.getParameter("telephone");
		abonnement = Abonnement.valueOf(request.getParameter("abonnement"));

		try {
			Membre membre = new Membre(id, nom, prenom, adresse, email, telephone, abonnement);
			membreService.update(membre);
		} catch (ServiceException e) {
			throw new ServletException(e);
		}

		response.sendRedirect("/LibraryManager/membre_details?id=" + id);
	}
}
