package com.excilys.librarymanager.servlet;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.excilys.librarymanager.exception.ServiceException;
import com.excilys.librarymanager.modele.Livre;
import com.excilys.librarymanager.modele.Membre;
import com.excilys.librarymanager.service.EmpruntService;
import com.excilys.librarymanager.service.EmpruntServiceImpl;
import com.excilys.librarymanager.service.LivreService;
import com.excilys.librarymanager.service.LivreServiceImpl;
import com.excilys.librarymanager.service.MembreService;
import com.excilys.librarymanager.service.MembreServiceImpl;

@SuppressWarnings("serial")
public class EmpruntAddServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		LivreService livreService = LivreServiceImpl.getInstance();
		MembreService membreService = MembreServiceImpl.getInstance();

		try {
			List<Livre> livres = livreService.getListDispo();
			request.setAttribute("livres", livres);

			List<Membre> membres = membreService.getListEmpruntPossible();
			request.setAttribute("membres", membres);
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/View/emprunt_add.jsp");
		rd.forward(request,response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EmpruntService empruntService = EmpruntServiceImpl.getInstance();
		int idMembre = -1;
		int idLivre = -1;
		LocalDate dateEmprunt = LocalDate.now();

		try {
			idMembre = Integer.parseInt(request.getParameter("idMembre"));
			idLivre = Integer.parseInt(request.getParameter("idLivre"));
		} catch(IllegalArgumentException e) {
			throw new ServletException("Identifiant de livre ou de membre invalide");
		}

		try {
			empruntService.create(idMembre, idLivre, dateEmprunt);
		} catch (ServiceException e) {
			throw new ServletException(e);
		}

		response.sendRedirect("/LibraryManager/emprunt_list");
	}
}
