package com.excilys.librarymanager.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.excilys.librarymanager.exception.ServiceException;
import com.excilys.librarymanager.modele.Emprunt;
import com.excilys.librarymanager.service.EmpruntService;
import com.excilys.librarymanager.service.EmpruntServiceImpl;

@SuppressWarnings("serial")
public class EmpruntListServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		boolean showAll = "all".equals(request.getParameter("show"));

		EmpruntService empruntService = EmpruntServiceImpl.getInstance();

		try {
			List<Emprunt> emprunts = null;

			if(showAll) {
				emprunts = empruntService.getList();
			} else {
				emprunts = empruntService.getListCurrent();
			}
			request.setAttribute("emprunts", emprunts);
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/View/emprunt_list.jsp");
		rd.forward(request,response);
	}
}
