package com.excilys.librarymanager.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.excilys.librarymanager.exception.ServiceException;
import com.excilys.librarymanager.modele.Livre;
import com.excilys.librarymanager.service.LivreService;
import com.excilys.librarymanager.service.LivreServiceImpl;

@SuppressWarnings("serial")
public class LivreDeleteServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = -1;
		LivreService livreService = LivreServiceImpl.getInstance();

		try {
			id = Integer.parseInt(request.getParameter("id"));
		} catch(IllegalArgumentException e) {
			throw new ServletException("Identifiant de livre invalide");
		}

		try {
			Livre livre = livreService.getById(id);
			request.setAttribute("livre", livre);
		} catch (ServiceException e) {
			throw new ServletException(e);
		}

		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/View/livre_delete.jsp");
		rd.forward(request,response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = -1;
		LivreService livreService = LivreServiceImpl.getInstance();

		try {
			id = Integer.parseInt(request.getParameter("id"));
		} catch(IllegalArgumentException e) {
			throw new ServletException("Identifiant de livre invalide");
		}

		try {
			livreService.delete(id);
		} catch (ServiceException e) {
			throw new ServletException(e);
		}

		response.sendRedirect("/LibraryManager/livre_list");
	}
}
