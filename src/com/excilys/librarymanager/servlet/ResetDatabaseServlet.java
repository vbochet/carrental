package com.excilys.librarymanager.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.excilys.librarymanager.utils.FillDatabase;

@SuppressWarnings("serial")
public class ResetDatabaseServlet extends HttpServlet {

	@SuppressWarnings("static-access")
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FillDatabase fillDatabase = new FillDatabase();

		try {
			fillDatabase.main(null);
		} catch (Exception e) {
			throw new ServletException("Une erreur s'est produite lors de la remise à zéro de la base de données", e);
		}
	}
}
