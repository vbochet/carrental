package com.excilys.librarymanager.modele;

public enum Abonnement {
	BASIC(2), PREMIUM(5), VIP(20);

	private int maxEmprunts;

	private Abonnement(int maxEmprunts) {
		this.maxEmprunts = maxEmprunts;
	}

	public int getMaxEmprunts() {
		return this.maxEmprunts;
	}
}
