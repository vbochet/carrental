package com.excilys.librarymanager.service;

import java.time.LocalDate;
import java.util.List;

import com.excilys.librarymanager.dao.EmpruntDao;
import com.excilys.librarymanager.dao.EmpruntDaoImpl;
import com.excilys.librarymanager.exception.DaoException;
import com.excilys.librarymanager.exception.ServiceException;
import com.excilys.librarymanager.modele.Emprunt;
import com.excilys.librarymanager.modele.Membre;

public class EmpruntServiceImpl implements EmpruntService {
	private static EmpruntServiceImpl instance;
	private EmpruntServiceImpl() {}

	private EmpruntDao empruntDao = EmpruntDaoImpl.getInstance();

	public static EmpruntServiceImpl getInstance() {
		if (instance == null) {
			instance = new EmpruntServiceImpl();
		}

		return instance;
	}

	public List<Emprunt> getList() throws ServiceException {
		try {
			return empruntDao.getList();
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la récupération de la liste des emprunts", e));
		}
	}

	public List<Emprunt> getListCurrent() throws ServiceException {
		try {
			return empruntDao.getListCurrent();
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la récupération de la liste des emprunts actuels", e));
		}
	}

	public List<Emprunt> getListCurrentByMembre(int idMembre) throws ServiceException {
		try {
			return empruntDao.getListCurrentByMembre(idMembre);
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la récupération de la liste des emprunts actuels pour le membre n°" + idMembre, e));
		}
	}

	public List<Emprunt> getListCurrentByLivre(int idLivre) throws ServiceException {
		try {
			return empruntDao.getListCurrentByLivre(idLivre);
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la récupération de la liste des emprunts actuels pour le livre n°" + idLivre, e));
		}
	}

	public Emprunt getById(int id) throws ServiceException {
		try {
			return empruntDao.getById(id);
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la récupération de l'emprunt", e));
		}
	}

	public void create(int idMembre, int idLivre, LocalDate dateEmprunt) throws ServiceException {
		try {
			empruntDao.create(idMembre, idLivre, dateEmprunt);
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la création de l'emprunt", e));
		}
	}

	public void returnBook(int id) throws ServiceException {
		try {
			Emprunt emprunt = empruntDao.getById(id);
			emprunt.setDateRetour(LocalDate.now());
			empruntDao.update(emprunt);
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la mise à jour de l'emprunt", e));
		}
	}

	public int count() throws ServiceException {
		try {
			return empruntDao.count();
		} catch (DaoException e) {
			throw(new ServiceException("Erreur SQL durant le comptage des emprunts", e));
		}
	}

	public boolean isLivreDispo(int idLivre) throws ServiceException {
		try {
			List<Emprunt> emprunts = empruntDao.getListCurrentByLivre(idLivre);
			for(Emprunt emprunt : emprunts) {
				if (emprunt.getDateRetour() == null) {
					return false;
				}
			}
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la récupération de la liste des emprunts pour le livre n°" + idLivre, e));
		}

		return true;
	}

	public boolean isEmpruntPossible(Membre membre) throws ServiceException {
		try {
			List<Emprunt> emprunts = getListCurrentByMembre(membre.getId());
			return emprunts.size() < membre.getAbonnement().getMaxEmprunts();
		} catch (ServiceException e) {
			throw(new ServiceException("Erreur durant la récupération de la liste des emprunts actuels du membre", e));
		}
	}
}
