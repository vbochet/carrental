package com.excilys.librarymanager.service;

import java.util.ArrayList;
import java.util.List;

import com.excilys.librarymanager.dao.LivreDao;
import com.excilys.librarymanager.dao.LivreDaoImpl;
import com.excilys.librarymanager.exception.DaoException;
import com.excilys.librarymanager.exception.ServiceException;
import com.excilys.librarymanager.modele.Livre;

public class LivreServiceImpl implements LivreService {
	private static LivreServiceImpl instance;
	private LivreServiceImpl() {}

	private LivreDao livreDao = LivreDaoImpl.getInstance();

	public static LivreServiceImpl getInstance() {
		if (instance == null) {
			instance = new LivreServiceImpl();
		}

		return instance;
	}

	public List<Livre> getList() throws ServiceException {
		try {
			return livreDao.getList();
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la récupération de la liste des livres", e));
		}
	}

	public List<Livre> getListDispo() throws ServiceException {
		EmpruntService empruntService = EmpruntServiceImpl.getInstance();
		try {
			List<Livre> livres = livreDao.getList();
			List<Livre> livresDispos = new ArrayList<>();
			for(Livre livre : livres) {
				if (empruntService.isLivreDispo(livre.getId())) {
					livresDispos.add(livre);
				}
			}
			return livresDispos;
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la récupération de la liste des livres disponibles", e));
		}
	}

	public Livre getById(int id) throws ServiceException {
		try {
			return livreDao.getById(id);
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la récupération du livre", e));
		}
	}

	public int create(String titre, String auteur, String isbn) throws ServiceException {
		if ("".equals(titre)) {
			throw new ServiceException("Un livre doit avoir un titre");
		}

		try {
			return livreDao.create(titre, auteur, isbn);
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la création du livre", e));
		}
	}

	public void update(Livre livre) throws ServiceException {
		if ("".equals(livre.getTitre())) {
			throw new ServiceException("Un membre doit avoir un titre");
		}

		try {
			livreDao.update(livre);
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la mise à jour du livre", e));
		}
	}

	public void delete(int id) throws ServiceException {
		try {
			livreDao.delete(id);
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la suppression du livre", e));
		}
	}

	public int count() throws ServiceException {
		try {
			return livreDao.count();
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant le comptage des livres", e));
		}
	}
}
