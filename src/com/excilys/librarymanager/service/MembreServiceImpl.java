package com.excilys.librarymanager.service;

import java.util.ArrayList;
import java.util.List;

import com.excilys.librarymanager.dao.MembreDao;
import com.excilys.librarymanager.dao.MembreDaoImpl;
import com.excilys.librarymanager.exception.DaoException;
import com.excilys.librarymanager.exception.ServiceException;
import com.excilys.librarymanager.modele.Membre;

public class MembreServiceImpl implements MembreService {
	private static MembreServiceImpl instance;
	private MembreServiceImpl() {}

	private static MembreDao membreDao = MembreDaoImpl.getInstance();
	
	public static MembreServiceImpl getInstance() {
		if (instance == null) {
			instance = new MembreServiceImpl();
		}

		return instance;
	}

	public List<Membre> getList() throws ServiceException {
		try {
			return membreDao.getList();
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la récupération de la liste des membres", e));
		}
	}

	public List<Membre> getListEmpruntPossible() throws ServiceException {
		EmpruntService empruntService = EmpruntServiceImpl.getInstance();
		try {
			List<Membre> membres = membreDao.getList();
			List<Membre> membresPossibles = new ArrayList<>();

			for(Membre membre : membres) {
				if (empruntService.isEmpruntPossible(membre)) {
					membresPossibles.add(membre);
				}
			}
			return membresPossibles;
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la récupération de la liste des membres", e));
		}
	}

	public Membre getById(int id) throws ServiceException {
		try {
			return membreDao.getById(id);
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la récupération du membre", e));
		}
	}

	public int create(String nom, String prenom, String adresse, String email, String telephone) throws ServiceException {
		if ("".equals(nom) || "".equals(prenom)) {
			throw new ServiceException("Un membre doit avoir un nom ET un prénom");
		}

		try {
			return membreDao.create(nom.toUpperCase(), prenom, adresse, email, telephone);
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la création du membre", e));
		}
	}

	public void update(Membre membre) throws ServiceException {
		if ("".equals(membre.getNom()) || "".equals(membre.getPrenom())) {
			throw new ServiceException("Un membre doit avoir un nom ET un prénom");
		}

		membre.setNom(membre.getNom().toUpperCase());

		try {
			membreDao.update(membre);
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la mise à jour du membre", e));
		}
	}

	public void delete(int id) throws ServiceException {
		try {
			membreDao.delete(id);
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant la suppression du membre", e));
		}
	}

	public int count() throws ServiceException {
		try {
			return membreDao.count();
		} catch (DaoException e) {
			throw(new ServiceException("Erreur durant le comptage des membres", e));
		}
	}

}
