package com.excilys.librarymanager.test;

import java.time.LocalDate;
import java.util.List;

import com.excilys.librarymanager.exception.ServiceException;
import com.excilys.librarymanager.modele.Membre;
import com.excilys.librarymanager.service.EmpruntService;
import com.excilys.librarymanager.service.EmpruntServiceImpl;
import com.excilys.librarymanager.service.LivreService;
import com.excilys.librarymanager.service.LivreServiceImpl;
import com.excilys.librarymanager.service.MembreService;
import com.excilys.librarymanager.service.MembreServiceImpl;
import com.excilys.librarymanager.modele.Emprunt;
import com.excilys.librarymanager.modele.Livre;
import com.excilys.librarymanager.utils.FillDatabase;

public class CliTest {
	public static void main(String[] args) {
		
		try {
			FillDatabase.main(null);
		} catch (Exception e3) {
			e3.printStackTrace();
		}
		
		
		MembreService membreService = MembreServiceImpl.getInstance();
		EmpruntService empruntService = EmpruntServiceImpl.getInstance();
		LivreService livreService = LivreServiceImpl.getInstance();

		try {
			empruntService.create(2, 1, LocalDate.of(2019, 3, 3));
		} catch (ServiceException e3) {
			e3.printStackTrace();
		}

		try {
			System.out.println("Livres (" + livreService.count() + "): \n -----------");
		} catch (ServiceException e2) {
			e2.printStackTrace();
		}

		try {
			List<Livre> livres = livreService.getList();
			for (Livre livre : livres) {
				System.out.println(livre);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		try {
			System.out.println("\nMembres ("+ membreService.count() +") : \n ---------");
		} catch (ServiceException e1) {
			e1.printStackTrace();
		}

		try {
			List<Membre> membres = membreService.getList();
			for (Membre membre : membres) {
				System.out.println(membre);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		try {
			System.out.println("\nEmprunts (" + empruntService.count() + ") : \n ---------");
		} catch (ServiceException e1) {
			e1.printStackTrace();
		}

		try {
			List<Emprunt> emprunts = empruntService.getList();
			for (Emprunt emprunt : emprunts) {
				System.out.println(emprunt);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		

		System.out.println("\nMembres au départ : \n ---------");

		try {
			List<Membre> membres = membreService.getList();
			for (Membre membre : membres) {
				System.out.println(membre);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		try {
			membreService.create("Bochet", "Vincent", "45 Avenue Carnot, 92400 CACHAN", "vbochet@excilys.com", "0123456789");
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		System.out.println("\nMembres après insertion : \n ---------");

		try {
			List<Membre> membres = membreService.getList();
			for (Membre membre : membres) {
				System.out.println(membre);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		try {
			Membre membre = new Membre(3, "Dubois", "Jean-Bob", "", "jbob@gmail.com", "");
			membreService.update(membre);
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		System.out.println("\nMembres après mise à jour : \n ---------");

		try {
			List<Membre> membres = membreService.getList();
			for (Membre membre : membres) {
				System.out.println(membre);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		

		try {
			membreService.delete(3);
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		System.out.println("\nMembres après suppression : \n ---------");

		try {
			List<Membre> membres = membreService.getList();
			for (Membre membre : membres) {
				System.out.println(membre);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		
		
		System.out.println("\nLivres au départ : \n ---------");

		try {
			List<Livre> livres = livreService.getList();
			for (Livre livre : livres) {
				System.out.println(livre);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		try {
			livreService.create("Le titre du livre", "A4", "AR-347-TX");
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		System.out.println("\nVéhicules après insertion : \n ---------");

		try {
			List<Livre> vehiculeList = livreService.getList();
			for (Livre v : vehiculeList) {
				System.out.println(v);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		try {
			Livre v = new Livre(6, "Citroën", "2CV", "");
			livreService.update(v);
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		System.out.println("\nVéhicules après mise à jour : \n ---------");

		try {
			List<Livre> vehiculeList = livreService.getList();
			for (Livre v : vehiculeList) {
				System.out.println(v);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		

		try {
			livreService.delete(6);
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		System.out.println("\nVéhicules après suppression : \n ---------");

		try {
			List<Livre> vehiculeList = livreService.getList();
			for (Livre v : vehiculeList) {
				System.out.println(v);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}

	}
}
