package com.excilys.librarymanager.ui;

import java.util.List;
import java.util.Scanner;

import com.excilys.librarymanager.exception.ServiceException;
import com.excilys.librarymanager.modele.Membre;
import com.excilys.librarymanager.service.EmpruntService;
import com.excilys.librarymanager.service.EmpruntServiceImpl;
import com.excilys.librarymanager.service.LivreService;
import com.excilys.librarymanager.service.LivreServiceImpl;
import com.excilys.librarymanager.service.MembreService;
import com.excilys.librarymanager.service.MembreServiceImpl;
import com.excilys.librarymanager.modele.Emprunt;
import com.excilys.librarymanager.modele.Livre;

public class Cli {
	public static void main(String[] args) {
		MembreService membreService = MembreServiceImpl.getInstance();
		EmpruntService empruntService = EmpruntServiceImpl.getInstance();
		LivreService livreService = LivreServiceImpl.getInstance();
		
		Scanner scIn = new Scanner(System.in);
		
		System.out.println("Que voulez-vous faire ?");
		System.out.println("1.  Voir la liste des membres");
		System.out.println("2.  Ajouter un membre");
		System.out.println("3.  Modifier une fiche membre");
		System.out.println("5.  Voir la liste des livres");
		System.out.println("6.  Ajouter un livre");
		System.out.println("7.  Réformer un livre");
		System.out.println("8.  Voir la liste des emprunts");
		System.out.println("9.  Ajouter une emprunts");
		System.out.println("10. Supprimer une emprunts");
		System.out.println("-------------------------------");
		System.out.print("Choix : ");
		
		int choice = 0;
		do {
			try {
				choice = scIn.nextInt();
			} catch(Exception e) {}
		} while (choice < 1 && choice > 10);

		scIn.close();

		switch (choice) {
		case 1:
			System.out.println("\nLISTE DES MEMBRES");
			System.out.println("-----------------");
			
			try {
				List<Membre> membres = membreService.getList();
				
				for (Membre membre : membres) {
					System.out.println(membre);
				}
			} catch (ServiceException e) {
				System.out.println("Une erreur est survenue lors de la récupération de la liste des membres !");
				e.printStackTrace();
			}
			
			break;
		case 2:
			break;
		case 3:
			break;
		case 4:
			break;
		case 5:
			System.out.println("\nLISTE DES LIVRES");
			System.out.println("-----------------");
			
			try {
				List<Livre> livres = livreService.getList();
				
				for (Livre livre : livres) {
					System.out.println(livre);
				}
			} catch (ServiceException e) {
				System.out.println("Une erreur est survenue lors de la récupération de la liste des livres !");
				e.printStackTrace();
			}
			
			break;
		case 6:
			break;
		case 7:
			break;
		case 8:
			System.out.println("\nLISTE DES EMPRUNTS");
			System.out.println("-----------------");
			
			try {
				List<Emprunt> emprunts = empruntService.getList();
				for (Emprunt emprunt : emprunts) {
					System.out.println(emprunt);
				}
			} catch (ServiceException e) {
				System.out.println("Une erreur est survenue lors de la récupération de la liste des emprunts !");
				e.printStackTrace();
			}
			
			break;
		case 9:
			break;
		case 10:
			break;
		}
	}
}
