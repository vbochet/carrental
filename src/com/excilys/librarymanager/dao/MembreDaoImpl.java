package com.excilys.librarymanager.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.excilys.librarymanager.exception.DaoException;
import com.excilys.librarymanager.modele.Abonnement;
import com.excilys.librarymanager.modele.Membre;
import com.excilys.librarymanager.persistence.ConnectionManager;

public class MembreDaoImpl implements MembreDao {
	private static MembreDaoImpl instance;
	private MembreDaoImpl() { }

	private static final String LIST_REQUEST	  = "SELECT id, nom, prenom, adresse, email, telephone, abonnement FROM membre ORDER BY nom, prenom;";
	private static final String GET_BY_ID_REQUEST = "SELECT id, nom, prenom, adresse, email, telephone, abonnement FROM membre WHERE id = ?;";
	private static final String INSERT_REQUEST	= "INSERT INTO membre(nom, prenom, adresse, email, telephone, abonnement) VALUES (?, ?, ?, ?, ?, ?);";
	private static final String UPDATE_REQUEST	= "UPDATE membre SET nom = ?, prenom = ?, adresse = ?, email = ?, telephone = ?, abonnement = ? WHERE id = ?;";
	private static final String DELETE_REQUEST	= "DELETE FROM membre WHERE id = ?;";
	private static final String COUNT_REQUEST	 = "SELECT COUNT(id) AS count FROM membre;";

	public static MembreDao getInstance() {
		if (instance == null) {
			instance = new MembreDaoImpl();
		}
		
		return instance;
	}

	@Override
	public List<Membre> getList() throws DaoException {
		List<Membre> membres = new ArrayList<>();

		try (Connection connection = ConnectionManager.getConnection();
			 Statement stmt = connection.createStatement();
			 ResultSet resultSet = stmt.executeQuery(LIST_REQUEST);) {
				while (resultSet.next()) {
					membres.add(new Membre(resultSet.getInt("id"), resultSet.getString("nom"), resultSet.getString("prenom"), resultSet.getString("adresse"), resultSet.getString("email"), resultSet.getString("telephone"), Abonnement.valueOf(resultSet.getString("abonnement"))));
				}
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant la récupération de la liste des membres", e);
		}

		return membres;
	}

	@Override
	public Membre getById(int id) throws DaoException {
		Membre membre = null;

		try (Connection connection = ConnectionManager.getConnection();
			 PreparedStatement stmt = connection.prepareStatement(GET_BY_ID_REQUEST);) {
			stmt.setInt(1,  id);
			try (ResultSet resultSet = stmt.executeQuery();) {
				if (resultSet.next()) {
					membre = new Membre(resultSet.getInt("id"), resultSet.getString("nom"), resultSet.getString("prenom"), resultSet.getString("adresse"), resultSet.getString("email"), resultSet.getString("telephone"), Abonnement.valueOf(resultSet.getString("abonnement")));
				}
			}
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant la récupération du membre", e);
		}

		return membre;
	}

	@Override
	public int create(String nom, String prenom, String adresse, String email, String telephone) throws DaoException {
		int id = -1;

		try (Connection connection = ConnectionManager.getConnection();
			 PreparedStatement stmt = connection.prepareStatement(INSERT_REQUEST, Statement.RETURN_GENERATED_KEYS);) {
			stmt.setString(1,  nom);
			stmt.setString(2,  prenom);
			stmt.setString(3,  adresse);
			stmt.setString(4,  email);
			stmt.setString(5,  telephone);
			stmt.setString(6,  Abonnement.BASIC.name());
			stmt.executeUpdate();

			try(ResultSet resultSet = stmt.getGeneratedKeys();) {
				if(resultSet.next()){
					id = resultSet.getInt(1);
				}
			}
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant la création du membre", e);
		}

		return id;
	}

	@Override
	public void update(Membre membre) throws DaoException {
		try (Connection connection = ConnectionManager.getConnection();
			 PreparedStatement stmt = connection.prepareStatement(UPDATE_REQUEST);) {
			stmt.setString(1,  membre.getNom());
			stmt.setString(2,  membre.getPrenom());
			stmt.setString(3,  membre.getAdresse());
			stmt.setString(4,  membre.getEmail());
			stmt.setString(5,  membre.getTelephone());
			stmt.setString(6,  membre.getAbonnement().name());
			stmt.setInt(7,  membre.getId());
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant la mise à jour du membre", e);
		}
	}

	@Override
	public void delete(int id) throws DaoException {
		try (Connection connection = ConnectionManager.getConnection();
			 PreparedStatement stmt = connection.prepareStatement(DELETE_REQUEST);) {
			stmt.setInt(1,  id);
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant la suppression du membre", e);
		}
	}

	@Override
	public int count() throws DaoException {
		int count = 0;

		try (Connection connection = ConnectionManager.getConnection();
			 Statement stmt = connection.createStatement();
			 ResultSet resultSet = stmt.executeQuery(COUNT_REQUEST);) {
				if (resultSet.next()) {
					count = resultSet.getInt("count");
				}
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant le comptage des membres", e);
		}

		return count;
	}
}
