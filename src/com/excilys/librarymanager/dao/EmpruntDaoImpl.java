package com.excilys.librarymanager.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.excilys.librarymanager.exception.DaoException;
import com.excilys.librarymanager.modele.Membre;
import com.excilys.librarymanager.modele.Emprunt;
import com.excilys.librarymanager.modele.Livre;
import com.excilys.librarymanager.persistence.ConnectionManager;

public class EmpruntDaoImpl implements EmpruntDao {
	private static EmpruntDaoImpl instance;
	private EmpruntDaoImpl() { }

	private static final String LIST_REQUEST 		 = "SELECT e.id AS id, idMembre, nom, prenom, adresse, email, telephone, idLivre, titre, auteur, isbn, dateEmprunt, dateRetour FROM emprunt AS e INNER JOIN membre ON membre.id = e.idMembre INNER JOIN livre ON livre.id = e.idLivre ORDER BY dateRetour DESC;";
	private static final String LIST_CURRENT_REQUEST = "SELECT e.id AS id, idMembre, nom, prenom, adresse, email, telephone, idLivre, titre, auteur, isbn, dateEmprunt, dateRetour FROM emprunt AS e INNER JOIN membre ON membre.id = e.idMembre INNER JOIN livre ON livre.id = e.idLivre WHERE dateRetour IS NULL;";
	private static final String LIST_CURRENT_FOR_MEMBRE_REQUEST = "SELECT e.id AS id, idMembre, nom, prenom, adresse, email, telephone, idLivre, titre, auteur, isbn, dateEmprunt, dateRetour FROM emprunt AS e INNER JOIN membre ON membre.id = e.idMembre INNER JOIN livre ON livre.id = e.idLivre WHERE dateRetour IS NULL AND membre.id = ?;";
	private static final String LIST_CURRENT_FOR_LIVRE_REQUEST = "SELECT e.id AS id, idMembre, nom, prenom, adresse, email, telephone, idLivre, titre, auteur, isbn, dateEmprunt, dateRetour FROM emprunt AS e INNER JOIN membre ON membre.id = e.idMembre INNER JOIN livre ON livre.id = e.idLivre WHERE dateRetour IS NULL AND livre.id = ?;";
	private static final String GET_BY_ID_REQUEST 	 = "SELECT e.id AS idEmprunt, idMembre, nom, prenom, adresse, email, telephone, idLivre, titre, auteur, isbn, dateEmprunt, dateRetour FROM emprunt AS e INNER JOIN membre ON membre.id = e.idMembre INNER JOIN livre ON livre.id = e.idLivre WHERE e.id = ?;";
	private static final String INSERT_REQUEST = "INSERT INTO emprunt(idMembre, idLivre, dateEmprunt, dateRetour) VALUES (?, ?, ?, ?);";
	private static final String UPDATE_REQUEST = "UPDATE emprunt SET idMembre = ?, idLivre = ?, dateEmprunt = ?, dateRetour = ? WHERE id = ?;";
	private static final String COUNT_REQUEST = "SELECT COUNT(id) AS count FROM emprunt;";

	public static EmpruntDao getInstance() {
		if (instance == null) {
			instance = new EmpruntDaoImpl();
		}

		return instance;
	}

	@Override
	public List<Emprunt> getList() throws DaoException {
		List<Emprunt> emprunts = new ArrayList<>();

		try (Connection connection = ConnectionManager.getConnection();
			 Statement stmt = connection.createStatement();
			 ResultSet resultSet = stmt.executeQuery(LIST_REQUEST);) {
				while (resultSet.next()) {
					LocalDate debut = resultSet.getDate("dateEmprunt").toLocalDate();
					LocalDate fin = null;
					if (null != resultSet.getDate("dateRetour")) {
						fin = resultSet.getDate("dateRetour").toLocalDate();
					}

					emprunts.add(new Emprunt(resultSet.getInt("id"), new Membre(resultSet.getInt("idMembre"), resultSet.getString("nom"), resultSet.getString("prenom"), resultSet.getString("adresse"), resultSet.getString("email"), resultSet.getString("telephone")), new Livre(resultSet.getInt("idLivre"), resultSet.getString("titre"), resultSet.getString("auteur"), resultSet.getString("isbn")), debut, fin));
				}
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant la récupération de la liste des emprunts", e);
		}

		return emprunts;
	}

	@Override
	public List<Emprunt> getListCurrent() throws DaoException {
		List<Emprunt> emprunts = new ArrayList<>();

		try (Connection connection = ConnectionManager.getConnection();
			 Statement stmt = connection.createStatement();
			 ResultSet resultSet = stmt.executeQuery(LIST_CURRENT_REQUEST);) {
				while (resultSet.next()) {
					LocalDate debut = resultSet.getDate("dateEmprunt").toLocalDate();
					LocalDate fin = null;
					emprunts.add(new Emprunt(resultSet.getInt("id"), new Membre(resultSet.getInt("idMembre"), resultSet.getString("nom"), resultSet.getString("prenom"), resultSet.getString("adresse"), resultSet.getString("email"), resultSet.getString("telephone")), new Livre(resultSet.getInt("idLivre"), resultSet.getString("titre"), resultSet.getString("auteur"), resultSet.getString("isbn")), debut, fin));
				}
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant la récupération de la liste des emprunts actuels", e);
		}

		return emprunts;
	}

	@Override
	public List<Emprunt> getListCurrentByMembre(int idMembre) throws DaoException {
		List<Emprunt> emprunts = new ArrayList<>();

		try (Connection connection = ConnectionManager.getConnection();
			 PreparedStatement stmt = connection.prepareStatement(LIST_CURRENT_FOR_MEMBRE_REQUEST);) {
			stmt.setInt(1, idMembre);
			try (ResultSet resultSet = stmt.executeQuery();) {
				while (resultSet.next()) {
					LocalDate debut = resultSet.getDate("dateEmprunt").toLocalDate();
					LocalDate fin = null;
					emprunts.add(new Emprunt(resultSet.getInt("id"), new Membre(resultSet.getInt("idMembre"), resultSet.getString("nom"), resultSet.getString("prenom"), resultSet.getString("adresse"), resultSet.getString("email"), resultSet.getString("telephone")), new Livre(resultSet.getInt("idLivre"), resultSet.getString("titre"), resultSet.getString("auteur"), resultSet.getString("isbn")), debut, fin));
				}
			}
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant la récupération de la liste des emprunts actuels pour le membre n°" + idMembre, e);
		}

		return emprunts;
	}

	@Override
	public List<Emprunt> getListCurrentByLivre(int idLivre) throws DaoException {
		List<Emprunt> emprunts = new ArrayList<>();

		try (Connection connection = ConnectionManager.getConnection();
			 PreparedStatement stmt = connection.prepareStatement(LIST_CURRENT_FOR_LIVRE_REQUEST);) {
			stmt.setInt(1, idLivre);
			try (ResultSet resultSet = stmt.executeQuery();) {
				while (resultSet.next()) {
					LocalDate debut = resultSet.getDate("dateEmprunt").toLocalDate();
					LocalDate fin = null;
					emprunts.add(new Emprunt(resultSet.getInt("id"), new Membre(resultSet.getInt("idMembre"), resultSet.getString("nom"), resultSet.getString("prenom"), resultSet.getString("adresse"), resultSet.getString("email"), resultSet.getString("telephone")), new Livre(resultSet.getInt("idLivre"), resultSet.getString("titre"), resultSet.getString("auteur"), resultSet.getString("isbn")), debut, fin));
				}
			}
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant la récupération de la liste des emprunts actuels pour le livre n°" + idLivre, e);
		}

		return emprunts;
	}

	@Override
	public Emprunt getById(int id) throws DaoException {
		Emprunt emprunt = null;

		try (Connection connection = ConnectionManager.getConnection();
			 PreparedStatement stmt = connection.prepareStatement(GET_BY_ID_REQUEST);) {
			stmt.setInt(1,  id);
			try (ResultSet resultSet = stmt.executeQuery();) {
				if (resultSet.next()) {
					LocalDate debut = resultSet.getDate("dateEmprunt").toLocalDate();
					LocalDate fin = null;
					if (null != resultSet.getDate("dateRetour")) {
						fin = resultSet.getDate("dateRetour").toLocalDate();
					}
					emprunt = new Emprunt(resultSet.getInt("idEmprunt"), new Membre(resultSet.getInt("idMembre"), resultSet.getString("nom"), resultSet.getString("prenom"), resultSet.getString("adresse"), resultSet.getString("email"), resultSet.getString("telephone")), new Livre(resultSet.getInt("idLivre"), resultSet.getString("titre"), resultSet.getString("auteur"), resultSet.getString("isbn")), debut, fin);
				}
			}
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant la récupération de l'emprunt", e);
		}

		return emprunt;
	}

	@Override
	public void create(int idMembre, int idLivre, LocalDate dateEmprunt) throws DaoException {
		try (Connection connection = ConnectionManager.getConnection();
			 PreparedStatement stmt = connection.prepareStatement(INSERT_REQUEST);) {
			stmt.setInt(1, idMembre);
			stmt.setInt(2, idLivre);
			stmt.setDate(3, Date.valueOf(dateEmprunt));
			stmt.setNull(4, Types.DATE); // lors de la création d'un emprunt, la date de retour est à null
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant la création de l'emprunt", e);
		}
	}

	@Override
	public void update(Emprunt emprunt) throws DaoException {
		try (Connection connection = ConnectionManager.getConnection();
			 PreparedStatement stmt = connection.prepareStatement(UPDATE_REQUEST);) {
			stmt.setInt(1, emprunt.getMembre().getId());
			stmt.setInt(2, emprunt.getLivre().getId());
			stmt.setDate(3, Date.valueOf(emprunt.getDateEmprunt()));
			if (emprunt.getDateRetour() != null) {
				stmt.setDate(4, Date.valueOf(emprunt.getDateRetour()));
			} else {
				stmt.setNull(4, Types.DATE);
			}
			stmt.setInt(5, emprunt.getId());
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant la mise à jour de l'emprunt", e);
		}
	}

	@Override
	public int count() throws DaoException {
		int count = 0;

		try (Connection connection = ConnectionManager.getConnection();
			 Statement stmt = connection.createStatement();
			 ResultSet resultSet = stmt.executeQuery(COUNT_REQUEST);) {
				if (resultSet.next()) {
					count = resultSet.getInt("count");
				}
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant le comptage des emprunts", e);
		}

		return count;
	}

}
