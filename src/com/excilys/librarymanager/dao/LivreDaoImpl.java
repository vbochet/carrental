package com.excilys.librarymanager.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.excilys.librarymanager.exception.DaoException;
import com.excilys.librarymanager.modele.Livre;
import com.excilys.librarymanager.persistence.ConnectionManager;

public class LivreDaoImpl implements LivreDao {
	private static LivreDaoImpl instance;
	private LivreDaoImpl() { }

	private static final String LIST_REQUEST = "SELECT id, titre, auteur, isbn FROM livre;";
	private static final String GET_BY_ID_REQUEST = "SELECT id, titre, auteur, isbn FROM livre WHERE id = ?;";
	private static final String INSERT_REQUEST = "INSERT INTO livre(titre, auteur, isbn) VALUES (?, ?, ?);";
	private static final String UPDATE_REQUEST = "UPDATE livre SET titre = ?, auteur = ?, isbn = ? WHERE id = ?;";
	private static final String DELETE_REQUEST = "DELETE FROM livre WHERE id = ?;";
	private static final String COUNT_REQUEST = "SELECT COUNT(id) AS count FROM livre;";

	public static LivreDao getInstance() {
		if (instance == null) {
			instance = new LivreDaoImpl();
		}

		return instance;
	}

	@Override
	public List<Livre> getList() throws DaoException {
		List<Livre> livres = new ArrayList<>();

		try (Connection connection = ConnectionManager.getConnection();
			 Statement stmt = connection.createStatement();
			 ResultSet resultSet = stmt.executeQuery(LIST_REQUEST);) {
				while (resultSet.next()) {
					livres.add(new Livre(resultSet.getInt("id"), resultSet.getString("titre"), resultSet.getString("auteur"), resultSet.getString("isbn")));
				}
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant la récupération de la liste des livres", e);
		}

		return livres;
	}

	@Override
	public Livre getById(int id) throws DaoException {
		Livre livre = null;

		try (Connection connection = ConnectionManager.getConnection();
			 PreparedStatement stmt = connection.prepareStatement(GET_BY_ID_REQUEST);) {
			stmt.setInt(1,  id);
			try (ResultSet resultSet = stmt.executeQuery();) {
				if (resultSet.next()) {
					livre = new Livre(resultSet.getInt("id"), resultSet.getString("titre"), resultSet.getString("auteur"), resultSet.getString("isbn"));
				}
			}
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant la récupération du livre", e);
		}

		return livre;
	}

	@Override
	public int create(String titre, String auteur, String isbn) throws DaoException {
		int id = -1;

		try (Connection connection = ConnectionManager.getConnection();
			 PreparedStatement stmt = connection.prepareStatement(INSERT_REQUEST, Statement.RETURN_GENERATED_KEYS);) {
			stmt.setString(1, titre);
			stmt.setString(2, auteur);
			stmt.setString(3, isbn);
			stmt.executeUpdate();

			try(ResultSet resultSet = stmt.getGeneratedKeys();) {
				if(resultSet.next()){
					id = resultSet.getInt(1);
				}
			}
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant la création du livre", e);
		}

		return id;
	}

	@Override
	public void update(Livre livre) throws DaoException {
		try (Connection connection = ConnectionManager.getConnection();
			 PreparedStatement stmt = connection.prepareStatement(UPDATE_REQUEST);) {
			stmt.setString(1,  livre.getTitre());
			stmt.setString(2,  livre.getAuteur());
			stmt.setString(3,  livre.getIsbn());
			stmt.setInt(4,  livre.getId());
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant la mise à jour du livre", e);
		}
	}

	@Override
	public void delete(int id) throws DaoException {
		try (Connection connection = ConnectionManager.getConnection();
			 PreparedStatement stmt = connection.prepareStatement(DELETE_REQUEST);) {
			stmt.setInt(1,  id);
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant la suppression du livre", e);
		}
	}

	@Override
	public int count() throws DaoException {
		int count = 0;

		try (Connection connection = ConnectionManager.getConnection();
			 Statement stmt = connection.createStatement();
			 ResultSet resultSet = stmt.executeQuery(COUNT_REQUEST);) {
				if (resultSet.next()) {
					count = resultSet.getInt("count");
				}
		} catch (SQLException e) {
			throw new DaoException("Erreur SQL durant le comptage des livres", e);
		}

		return count;
	}

}
